# NB! when updating make sure the version is in sync with:
# * rasa version in requirements.txt
# * RASA_VERSION and RASA_X_VERSION  in .github/workflows/continuous-deployment.yml
# Pull SDK image as base image
FROM rasa/rasa:latest


# Change to root user to install dependencies
USER root

RUN apt-get update -qq && \
  apt-get install -y --no-install-recommends \
  python3 \
  python3-venv \
  python3-pip \
  python3-dev \
  # required by psycopg2 at build and runtime
  libpq-dev \
  # required for health check
  curl \
  # required to change locales
  locales \
  && apt-get autoremove -y
  
# Make sure that all security updates are installed
RUN apt-get update && apt-get dist-upgrade -y --no-install-recommends

# Download spacy language data
RUN python -m pip install rasa[spacy] && \
    python -m spacy download fr_core_news_md

# Install locales
RUN locale-gen fr_FR.UTF-8

# Don't use root user to run code
USER 1001

# Start the action server
EXPOSE 5005
ENTRYPOINT ["rasa"]
CMD ["--help"]
